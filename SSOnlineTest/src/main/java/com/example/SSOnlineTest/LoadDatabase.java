package com.example.SSOnlineTest;

import lombok.extern.slf4j.Slf4j;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
class LoadDatabase {

  @Bean
  CommandLineRunner initDatabase(TransactionRepository repository) {
    return args -> {
      log.info("Preloading " + repository.save(new Transaction(100, "debit", 0)));
      log.info("Preloading " + repository.save(new Transaction(10, "credit", 0)));
    };
  }
}