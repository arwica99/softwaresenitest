package com.example.SSOnlineTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SsOnlineTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SsOnlineTestApplication.class, args);
	}

}
