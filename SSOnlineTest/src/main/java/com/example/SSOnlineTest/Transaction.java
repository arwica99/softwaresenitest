package com.example.SSOnlineTest;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
class Transaction {

  private @Id @GeneratedValue Long id;
  private int amount;
  private String type;
  private int parentId;

  Transaction() {}

  Transaction(int amount, String type, int parentId) {
	this.amount = amount;
	this.type = type;
	this.parentId = parentId;
  }
  
  public String getAmount() {
    return this.amount;
  }
  
  public String getType() {
    return this.type;
  }
  
  public String getParent() {
    return this.parentId;
  }

  public void setAmount(int amount) {
    this.amount = amount;
  }

  public void setType(String type) {
    this.type = type;
  }

  public void setParent(int parentId) {
    this.parentId = parentId;
  }
}