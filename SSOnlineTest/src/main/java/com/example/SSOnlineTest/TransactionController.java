package com.example.SSOnlineTest;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
class TransactionController {

  private final TransactionRepository repository;

  TransactionController(TransactionRepository repository) {
    this.repository = repository;
  }

  // Get All
  @GetMapping("/transactionservice/transactions")
  List<Transaction> all() {
    return repository.findAll();
  }

  // Get One data by id
  @GetMapping("/transactionservice/transaction/{id}")
  Transaction one(@PathVariable Long id) {

    return repository.findById(id).orElseThrow(() -> new DataNotFoundException(id));
  }

  // Get One data by type
  @GetMapping("/transactionservice/transaction/type/{type}")
  Transaction one(@PathVariable String type) {

    return repository.findByType(type).orElseThrow(() -> new DataNotFoundException(id));
  }

  // Get Summary
  @GetMapping("/transactionservice/sum/{parentId}")
  Transaction one(@PathVariable int parentId) {
	int totalAmount = 0;

    return repository.findByParent(parentId)
		.map(transaction -> {
			totalAmount += transaction.getAmount();
			
			return totalAmount;
		})
		.orElseThrow(() -> new DataNotFoundException(id));
  }

  // Change Status
  @PutMapping("/transactionservice/transaction/{id}")
  Transaction changeTransaction(@RequestBody Transaction newTransaction, @PathVariable Long id) {

    return repository.findById(id)
      .map(transaction -> {
        transaction.setAmount(newTransaction.getAmount());
        transaction.setType(newTransaction.getType());
        transaction.setParent(newTransaction.getParent());
        repository.save(transaction);
		
		return "{\"status\":\"OK\"}";
      })
      .orElseGet(() -> {
        newTransaction.setId(id);
        repository.save(newTransaction);
		
		return "{\"status\":\"OK\"}";
      });
  }

  // Store or Create New
  @PostMapping("/transactionservice/transaction")
  Transaction newTransaction(@RequestBody Transaction newTransaction) {
    return repository.save(newTransaction);
  }

  // Updating
  @PutMapping("/transaction/{id}")
  Transaction updateTransaction(@RequestBody Transaction newTransaction, @PathVariable Long id) {

    return repository.findById(id)
      .map(transaction -> {
        transaction.setAmount(newTransaction.getAmount());
        transaction.setType(newTransaction.getType());
        transaction.setParent(newTransaction.getParent());
        return repository.save(transaction);
      })
      .orElseGet(() -> {
        newTransaction.setId(id);
        return repository.save(newTransaction);
      });
  }

  // Deleting
  @DeleteMapping("/transaction/{id}")
  void deleteTransaction(@PathVariable Long id) {
    repository.deleteById(id);
  }
}