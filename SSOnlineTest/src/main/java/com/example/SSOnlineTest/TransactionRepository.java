package com.example.SSOnlineTest;

import org.springframework.data.jpa.repository.JpaRepository;

interface TransactionRepository extends JpaRepository<Transaction, Long> {
  List<Transaction> findByType(String type);
  List<Transaction> findByParent(int parentId);

}